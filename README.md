# 微门禁小程序项目

#### 介绍
微门禁小程序项目,免费开源;
(申明:源码提供给大家,因时间关系,不免费提供安装配置,不提供小程序编译对接上传等咨询服务。)

#### 软件架构
软件架构说明
wxapp.wmj.com.cn为管理后台源码。
wxapp_miniprogram为微信小程序源码。

#### 管理后台安装教程

1.  基于ThinkPHP6.0开发，建议使用宝塔面板
2.  PHP7.2+
3.  Mysql5.5+
4.  修改数据库连接在根目录下.env文件,数据库脚本为weimenjin_miniprogram_db.sql
5.  后台超级管理员admin,默认密码wmj123456
6.  运行目录为public


#### 小程序的api文档

地址：https://wxapp.wmj.com.cn/doc/

#### 使用说明

详情见：http://doc.wmj.com.cn/web/#/1?page_id=34